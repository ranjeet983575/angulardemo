import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.scss']
})
export class ReactiveFormComponent implements OnInit {




  constructor(private fb: FormBuilder) { }

  productForm = this.fb.group({
    email: ['',Validators.required],
    password: ['',Validators.required],
    cpassword: ['',Validators.required],
    pin: ['']
  });

  submit():void{

    console.log(this.productForm.get('email'));
   
    
    console.log(this.productForm);
    
    console.log(this.productForm.value);
    
  }





  ngOnInit(): void {
  }

}
